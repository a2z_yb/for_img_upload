﻿---
categories: [개발 환경, Guide]
tags: [WSL]
---

# WSL2 설치 및 사용 가이드
이 글은 WSL을 사용하려고 하는 분들에게 최소한의 설명으로 쉽게 따라올 수 있도록 작성하였습니다.

> 본 글은 다음의 Microsoft docs 공식 문서를 참고하였습니다. ref.: [link](https://docs.microsoft.com/ko-kr/windows/wsl/install-manual#step-6---install-your-linux-distribution-of-choice)

### TL;DR
- 관리자 권한으로 PowerShell 실행
- Linux용 Windows 하위 시스템 사용 설정<br />
$  dism.exe /online /enable-feature /featurename:Microsoft-Windows-Subsystem-Linux /all /norestart
- Virtual Machine 기능 사용 설정<br />
$ dism.exe /online /enable-feature /featurename:Microsoft-Windows-Subsystem-Linux /all /norestart
- 리눅스 최신 커널 업데이트 [Download](https://wslstorestorage.blob.core.windows.net/wslblob/wsl_update_x64.msi)
- WSL2를 기본 버전으로 설정<br />
$ wsl --set-default-version 2
- 원하는 Linux 배포판 설치 [Microsoft Store](https://aka.ms/wslstore)
- Linux 계정 설정 (username, password)
<br />
<br />

## 0. 설치 전 확인 사항

본 글은 **Windows 10** 환경에서 **WSL2**를 이용하여 **Ubuntu 20.04 LST**을 사용하는 것을 목표로 합니다.

**1) 제 환경은 아래와 같습니다.**
> 에디션: Windows 10 Pro
> 버전: 21H2
> OS 빌드: 19044.1706
<br />
**2) 윈도우 최신 버전 확인**
* win키 ➡ "PC 정보" 검색<br />
![img_1.png](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL/img_1.png)

* '시스템 - 정보 - Windows 사양' 확인
![img_2.png](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL/img_2.png)
> x64 시스템의 경우, WSL 2를 사용하려면 Windows 10, 버전 1903 이상, 빌드 18362 이상이 필요합니다.

* 현재 Windows의 버전이 20H1, 20H2, 21H1, 21H2 중 하나가 아니고, 빌드가 18362 이하라면, 업데이트를 해주시면 됩니다.
* win키 ➡ "업데이트 확인" 검색<br />
![img_3.png](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL/img_3.png)

* (업데이트 필요 시) '업데이트 및 보안 - Windows 업데이트'에서 업데이트 진행
![img_4.png](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL/img_4.png)
<br />
<br />

## 1. WSL2 설치 및 설정
**1) Windows PowerShell을 ==관리자 권한==으로 실행합니다.**
* win 키 ➡ 'powershell' 검색
* 마우스 오른쪽 버튼 클릭 ➡ 관리자 권한으로 실행<br />
![img_5.png](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL/img_5.png)
* 사용자 계정 컨트롤 ➡ '예' 클릭
* 관리자 권한으로 시작하면 위에 **관리자: Windows PowerShell** 이 써있고, 경로도 **system32** 입니다.
![img_6.png](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL/img_6.png) ![img_7.png](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL/img_7.png)
* 좌측의 그림은 관리자 권한으로 PowerShell을 실행했을 경우이고, 우측의 그림은 그냥 실행한 경우입니다. 관리자 권한으로 실행을 했는지 다시 한번 확인해 주세요.
<br />

**2) Linux용 Windows 하위 시스템 사용 설정**
* 다음 명령을 관리자 권한 PowerShell에 입력해 주세요.
![img_8.png](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL/img_8.png) ![img_9.png](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL/img_9.png)

      dism.exe /online /enable-feature /featurename:Microsoft-Windows-Subsystem-Linux /all /norestart
<br />

**3) Virtual Machine 기능 사용 설정**
* 다음 명령을 관리자 권한 PowerShell에 입력해 주세요.
![img_10.png](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL/img_10.png) ![img_11.png](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL/img_11.png)

      dism.exe /online /enable-feature /featurename:VirtualMachinePlatform /all /norestart

* 위 메세지(작업을 완료했습니다.)를 확인 후, 반드시 **재부팅**해주세요.
<br />

**4) 리눅스 최신 커널 업데이트**
* 다음 링크에서 Linux 커널 업데이트 패키지를 다운로드 합니다. [Download](https://wslstorestorage.blob.core.windows.net/wslblob/wsl_update_x64.msi)<br />
![img_12.png](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL/img_12.png)
* 다운받은 파일(wsl_update_x64.msi)을 실행합니다.
* 사용자 계정 컨트롤 ➡ '예' 클릭<br />
![img_13.png](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL/img_13.png)
* 'Finish' 클릭
<br />

**5) WSL2를 기본 버전으로 설정**
* WSL1과 WSL2의 차이점은 다음 문서를 확인해주세요 [링크](https://docs.microsoft.com/ko-kr/windows/wsl/compare-versions)
* 다음 명령을 관리자 권한 PowerShell에 입력해 주세요.
![img_14.png](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL/img_14.png) ![img_15.png](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL/img_15.png)

      wsl --set-default-version 2


* **"WSL 2와의 주요 차이점에 대한 자세한 내용은 https://<dummy>aka<dummy>.ms/wsl2를 참조하세요"**라는 메시지를 확인해 주세요.
* 위의 메시지가 뜨지 않는다면 제대로 설정되지 않은 것입니다. 이 경우 6)을 참고해주세요.
<br />

**6) (설정이 제대로 안됐을 경우)**
* win 키 ➡ 'Windows 기능 켜기/끄기' 검색<br />
![img_16.png](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL/img_16.png)
* 'Windows 기능 켜기/끄기'에서  'Linux용 Windows 하위 시스템'과 '가상 머신 플랫폼'의 체크 박스를 확인해 주세요.
* **체크가 안되어 있으면** 두 개 모두 체크 하고 **'확인'** 클릭 ➡ **'다시 시작'** 클릭
* **체크가 되어 있으면** 체크 해제 후 **'확인'** ➡ **다시 시작 안함** 클릭 ➡ 다시 체크 후 **'확인'** ➡ **'다시 시작'** 클릭
![img_17.png](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL/img_17.png) ![img_18.png](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL/img_18.png) ![img_19.png](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL/img_19.png)

>  'Linux용 Windows 하위 시스템'과 '가상 머신 플랫폼'은 각각 2)와 3)의 과정으로 해당 기능을 활성화 하지만, 제대로 수행되지 않는 경우가 많은 것 같습니다. 이 경우, 과정 6)을 참고하시면 대부분 해결 가능합니다.
<br />

**7) 원하는 Linux 배포판 설치**
* [Microsoft Store](https://aka.ms/wslstore)를 열고, 원하는 Linux 배포판을 검색합니다. 
![img_20.png](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL/img_20.png)

* 우측 상단의 **다운로드**를 클릭합니다.
<br />

**8) Linux 계정 설정**
* 7)에서 다운로드가 완료되면, win 키를 눌러 설치된 Linux 배포판을 실행합니다.<br />
![img_21.png](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL/img_21.png)
* "Installing, this may take a few minutes..."메시지가 나온 후, 말 그대로 몇 분 기다려야 됩니다. 아무것도 안 뜬다고 당황하지 마세요.
* 다음을 입력해 주세요. 
  - Enter new UNIX username: 사용자 계정 이름 입력
  - New password: 사용할 비밀번호 입력  
  - Retype  new  password: 확인을 위하여 비밀번호 다시 입력
![img_22.png](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL/img_22.png)
<br />
<br />

## 2. WSL2 설치 확인
* 8)까지의 과정을 따라오셨으면 Windows에서 Linux를 사용하기 위한 설치와 설정이 완료된 것입니다.
* 제대로 설치 및 설정이 됐는지 확인하려면, 다음 명령을 PowerShell에 입력해 주세요. (관리자 권한이 아니어도 됩니다.)
![img_23.png](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL/img_23.png)

      wsl --list --verbose
   또는,
   
      wsl -l -v   


* **NAME**은 설치한 Linux 배포판입니다.
* **STATE**는 WSL2로 Linux 배포판을 
실행 중이면,  **Running**
실행 중이 아니라면, **Stopped**
이라고 뜹니다.
* **VERSION**에서 **2**는 WSL2을 의미합니다.
<br />
<br />

## 3. WSL2 사용 팁
**Windows Terminal**
 * [Microsoft Store](https://aka.ms/wslstore)에서 Windows Terminal을 설치해 주세요.
 * 리눅스의 그것과 거의 비슷합니다.
 * 없어도 되지만, 있으면 편리합니다. <br />
![img_24.png](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL/img_24.png)

* 처음으로 Windows Terminal을 실행하면 Windows PowerShell이 실행됩니다.
* 상단에 + 옆 v를 눌러 **설정**을 클릭합니다.
* '시작-기본 프로필'에서 Ubuntu를 기본 설정으로 하고 저장을 클릭합니다.
* 그러면, 다음 실행부터 Ubuntu terminal이 실행됩니다.<br />
![img_25.png](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL/img_25.png) ![img_26.png](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL/img_26.png) ![img_27.png](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL/img_27.png) ![img_28.png](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL/img_28.png)

---

**윈도우-리눅스 디렉토리 접근**
* Windows Terminal에서 다음 명령어를 입력해 주세요.
![img_29.png](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL/img_29.png)

      explorer.exe .
 
* Windows 탐색기가 열리면 경로를 복사해 주세요.<br />
![img_30.png](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL/img_30.png)

* 바탕화면에서 마우스 "오른쪽 버튼 클릭-새로 만들기-바로가기(S)"를 클릭해 주세요.<br />
![img_31.png](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL/img_31.png)

* '항목 위치 입력'에 복사한 경로를 입력하고, '다음'을 클릭해 주세요.<br />
![img_32.png](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL/img_32.png)

* 바로가기의 이름을 지정하고, '마침'을 클릭해 주세요.<br />
![img_33.png](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL/img_33.png)

* '바로가기'를 생성하면 언제든지 리눅스 경로에 접근할 수 있습니다.<br />
![img_34.png](https://gitlab.com/a2z_yb/for_img_upload/-/raw/main/WSL/img_34.png)

---

**WSL2에서 GUI 사용 방법**


(작성 전)

---

**WSL2에서 딥러닝 개발 환경 구축하기**

(작성 전)

